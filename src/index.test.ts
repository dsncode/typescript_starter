import Demo from "./index"
const { test, expect } = require("@jest/globals");


test("class should have a name", () => {

    let x = new Demo("hello");


    expect(x.name).toBe("hello")
})